﻿<%@ Page Title="Delete Files Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="DeleteFiles.aspx.cs" Inherits="Generate_270_Files.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    <p>
      <asp:Button ID="BtnDeleteFiles" runat="server" Text="Delete Files" 
        onclick="BtnDeleteFiles_Click" />
    </p>
     <hr />
    <asp:Label ID="FilesCount" runat="server" Text=""></asp:Label>
   <asp:Label ID="lblMessage" runat="server" Visible="False" ForeColor="Red" EnableViewState="False"></asp:Label>
</asp:Content>
