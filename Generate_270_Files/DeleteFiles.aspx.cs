﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Generate_270_Files
{
  public partial class About : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        GetFileCount();
      }
      catch (Exception ex)
      {
        lblMessage.Text = ex.InnerException.ToString();
        lblMessage.Visible = true;
      }
    }

    protected void BtnDeleteFiles_Click(object sender, EventArgs e)
    {
      try
      {
        GetAndDisplayFiles();
      }
      catch (Exception ex)
      {
        lblMessage.Text = ex.InnerException.ToString();
        lblMessage.Visible = true;
      }
    }

    private void GetAndDisplayFiles()
    {
      Array.ForEach(Directory.GetFiles(Server.MapPath("FilesLocation\\"), "*.raw"), File.Delete);
      GetFileCount();
    }

    private void GetFileCount()
    {
      DirectoryInfo dir = new DirectoryInfo(Server.MapPath("FilesLocation\\"));
      FileInfo[] files = dir.GetFiles("*.raw");

      FilesCount.Text = "Current file(s) Count  : " + files.Length.ToString();
    }
  }
}
