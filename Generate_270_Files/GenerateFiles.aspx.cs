﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;

namespace Generate_270_Files
{
  public partial class _Default : System.Web.UI.Page
  {
    #region Fields

    private string fileExtension = @".raw";
    private int patientSSN = Convert.ToInt32(111223333);
    private int insuraneHolderSSN = Convert.ToInt32(111223333);   // Insurance holder SSN is Patient SSN, Need to know deeply if same.
    private string firstNameList = @"firstNameList";
    private string lastNameList = @"lastNameList";
    private char currentChar = 'a';
    private Random randomNumber = new Random((int)DateTime.Now.Ticks);  // To reduce the same number generation used Ticks.

    #endregion

    #region Class Methods

    protected void Page_Load(object sender, EventArgs e)
    {
      
      try
      {
        FileInfo[] generatedFiles = GetFiles();
        if (generatedFiles.Length > 0)
        {
          FilesCount.Text = "Current file(s) count  : " + generatedFiles.Length.ToString();
          GetAndDisplayFiles();
        }
      }
      catch (Exception ex)
      {
        lblMessage.Text = ex.InnerException.ToString();
        lblMessage.Visible = true;
      }
    } 

    protected void BtnGenerateFiles_Click(object sender, EventArgs e)
    {
      try
      {        
        string inputFileCount = txtFileCount.Text.ToString();
        if (string.IsNullOrWhiteSpace(inputFileCount) || Convert.ToInt32(inputFileCount) < 1 || Convert.ToInt32(inputFileCount) > 500 || DDLPayer.SelectedIndex == 0)
        {
          lblMessage.Text = "File count must be between (1-500) & Payer Option must be selected. ";
          lblMessage.Visible = true;
        }
        else
        {
          VerifyLastNumberGeneratedFile();

          int fileCount = Convert.ToInt32(inputFileCount);
          int lastNumberGenerated = GetLastNumberGenerated();

          patientSSN = lastNumberGenerated;
          insuraneHolderSSN = lastNumberGenerated;

          List<string> firstNamesList = GetNamesList(firstNameList);
          List<string> lastNamesList = GetNamesList(lastNameList);

          StringBuilder sb = new StringBuilder();
          ArrayList al = new ArrayList();

          #region Generate and store the files as per the input file count.
          
          for (int count = 0; count < fileCount; count++)
          {
            int randomlySelectedNumber = randomNumber.Next(0, firstNamesList.Count); // FirstName and LastName count should be equal.
          
            string currentFirstName = firstNamesList[randomlySelectedNumber];
            string currentLastName = lastNamesList[randomlySelectedNumber];

            string insuranceHolder = currentLastName;  // Insurance-holder and FirstName are equals to last name of patient name.
            string insuranceHolderFirstName = currentLastName;

            string fileName = GenerateFileName(currentFirstName, currentLastName);
            char nextChar = GetSequentialLetter(count, currentChar);

            GenerateAndStoreFile(fileName, nextChar.ToString() + currentFirstName, nextChar.ToString() + currentLastName, patientSSN, insuraneHolderSSN, insuranceHolderFirstName, DDLPayer.SelectedValue);

            currentChar = nextChar;  // To store the current character for the next iteration.

            patientSSN += 1;
            insuraneHolderSSN += 1;
          }

          #endregion

          UpdateLastNumberGenerated(patientSSN);
          GetAndDisplayFiles();
        }
      }
      catch (Exception ex)
      {
        lblMessage.Text = ex.InnerException.ToString(); // Added inner exception and customErrors mode=Off to know the exact issues. After these will be modified.
        lblMessage.Visible = true;
      }
      finally
      {
        txtFileCount.Text = "";
        DDLPayer.SelectedIndex = 0;
      }
    }   

    #endregion

    #region Priave Methods

    private FileInfo[] GetFiles()
    {
      return new DirectoryInfo(Server.MapPath("FilesLocation\\")).GetFiles("*.raw");
    }

    private void GetAndDisplayFiles()
    {
      StringBuilder sb = new StringBuilder();
      FileInfo[] files = GetFiles();

      foreach (FileInfo fileInfo in files)
      {
        sb.Append(fileInfo.Name.ToString() + "<br/>");
      }

      FilesCount.Text = "Current file(s) Count  : " + files.Length.ToString();

      DisplayFilesContainer.Text = sb.ToString();
    }

    private void VerifyLastNumberGeneratedFile()
    {
      DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath("InPutFileLocator\\"));
      FileInfo[] outPutFolderfiles = directoryInfo.GetFiles("LastNumberGenerated.txt");

      if (outPutFolderfiles.Length == 0)
      {
        CreateLastNumberStoredFile();
      }
    }

    private void CreateLastNumberStoredFile()
    {
      string fileContent = @"111223333";
      string filesInInPutFileLocator = Server.MapPath("InPutFileLocator\\LastNumberGenerated.txt");

      using (FileStream stream = new FileStream(filesInInPutFileLocator, FileMode.Create, FileAccess.ReadWrite))
      {
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
          writer.Write(fileContent);
          writer.Close();
        }
      }
    }

    private int GetLastNumberGenerated()
    {
      string strLastNumber = File.ReadAllText(LastNumberGenerateFilePath());

      int lastNumber = 111223333;

      if (!string.IsNullOrWhiteSpace(strLastNumber))
        lastNumber = Convert.ToInt32(strLastNumber);

      return lastNumber;
    }

    private List<string> GetNamesList(string nameType)
    {
      FileInfo[] files = null;

      if (nameType.Equals(firstNameList))
      {
        files = new DirectoryInfo(Server.MapPath("InPutFileLocator\\")).GetFiles("FirstNamesContainer.txt");
      }
      else
      {
        files = new DirectoryInfo(Server.MapPath("InPutFileLocator\\")).GetFiles("LastNamesContainer.txt");
      }

      List<string> namesList = (File.ReadAllLines(files[0].FullName)
          .Where(line => !string.IsNullOrEmpty(line))
          .ToList<string>());

      return namesList;
    }

    private string GenerateFileName(string currentFirstName, string currentLastName)
    {
      return @"_Mock Fake Payer 1_" + currentFirstName + "_" + currentLastName + "_20120520201409_" + patientSSN + fileExtension; // TBD filename content;
    }

    public char GetSequentialLetter(int count, char initialChar)
    {
      if (count == 0)
      {
        initialChar = 'a';
        return initialChar;
      }

      if (initialChar == 'z')
      {
        initialChar = 'a';
      }
      else
      {
        initialChar++;
      }

      return initialChar;
    }

    private void GenerateAndStoreFile(string fileName, string patientFirstName, string patientLastName, int patientSSN, int insuraneHolderSSN, string insuranceHolderFirstName, string payerOption)
    {
      string fileContent = @"H000727137|" + patientLastName + "|" + patientFirstName + "|19750221|F|" + patientSSN + "|80354120404||00205||20140421|20140421||||H1411101314|" + insuraneHolderSSN + "|" + patientLastName + "|" + patientFirstName + "|FL||||||319049||||FL|||38261holmes|||||||005010X279A1|||||||||||||||||||||||" + insuranceHolderFirstName + "|270|||||00205||||||" + payerOption + "||||||||01013117|20140421235017||||" + insuranceHolderFirstName + "|||63298-2833|63298-2833||47";
      string filesStoreLocation = Server.MapPath("FilesLocation\\" + fileName);

      using (FileStream stream = new FileStream(filesStoreLocation, FileMode.Create))
      {
        using (BinaryWriter writer = new BinaryWriter(stream))
        {
          writer.Write(fileContent);
          writer.Close();
        }
      }
    }

    private void UpdateLastNumberGenerated(int newValue)
    {
      string strFilePath = LastNumberGenerateFilePath();

      using (FileStream stream = new FileStream(strFilePath, FileMode.Create))
      {
        using (BinaryWriter writer = new BinaryWriter(stream))
        {          
          writer.Write(newValue.ToString());
          writer.Close();
        }
      }
    }

    private string LastNumberGenerateFilePath()
    {
      return new DirectoryInfo(Server.MapPath("InPutFileLocator\\")).GetFiles("LastNumberGenerated.txt")[0].FullName;
    }

    #endregion    
  }
}
