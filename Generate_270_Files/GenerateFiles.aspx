﻿<%@ Page Title="Generate Files Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="GenerateFiles.aspx.cs" Inherits="Generate_270_Files._Default" %>
   
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<table width="45%">
<tr>
<td colspan="2"><asp:Label ID="lblMessage" runat="server" Visible="False" ForeColor="Red" EnableViewState="False"></asp:Label></td>
</tr>
<tr>
<td><asp:Label ID="lblFileCount" runat="server" Text="Enter file count "></asp:Label>:</td>
<td><asp:TextBox ID="txtFileCount" runat="server" EnableViewState="False"></asp:TextBox></td>
</tr>
<tr>
<td><asp:Label ID="lblPayerOption" runat="server" Text="Select Payer "></asp:Label>:</td>
<td><asp:DropDownList ID="DDLPayer" runat="server">
       <asp:ListItem>Payer Options</asp:ListItem>
       <asp:ListItem>MEDICARE</asp:ListItem>
       <asp:ListItem>RED-AENTX</asp:ListItem>
     </asp:DropDownList></td>
</tr>
<tr>
<td><asp:Button ID="BtnGenerateFiles" runat="server" Text="Generate Files" onclick="BtnGenerateFiles_Click" /></td>
<td><asp:Label ID="FilesCount" runat="server" Text=""></asp:Label>
</td>
</tr>
</table>
<hr />
<div style="height:600px;overflow:auto"> 
     <asp:Literal ID="DisplayFilesContainer" runat="server" ></asp:Literal>
   </div>
</asp:Content>
